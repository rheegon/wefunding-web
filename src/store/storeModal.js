

const modal = {
  namespaced: true,
  state: {
    items: [
      // {
      //   component: "AuthLogin",
      //   size: "small || big"
      // }
    ]
  },
  getters: {
    GET_ITEMS: ( state ) => {
      return state.items;
    }
  },
  mutations: {
    ADD_ITEM( state, payload ){
      state.items.push( payload );
    },
    REMOVE_ITEM( state, payload ){
      state.items = state.items.filter( o => o.component != payload.component );
    }
  }
}

export default modal
