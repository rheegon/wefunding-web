import Vue from "vue";
import Vuex from "vuex";
import config from "@/config";

import auth from "./storeAuth";
import userData from "./storeUserData";
import alert from "./storeAlert";
import modal from "./storeModal";
import dialog from "./storeDialog";

Vue.use( Vuex );

const store = new Vuex.Store({
  modules: {
    alert,
    modal,
    dialog,
    auth,
    userData
  },
  state: {
    config: config,
  },
  mutations: {

  },
  actions: {
    blockRefreshOnScroll({ state }, payload ){
      if( payload.parent && payload.child ){
        // BLOCK REFRESH ON SCROLL
        var clickEvent = config.isTouchDevice ? "touchstart" : "mousedown";
        payload.parent.scrollTop = 1;
        payload.parent.addEventListener( "scroll", e => {
          if( payload.parent.scrollTop == 0 ){
            payload.parent.scrollTop = 1;
          }else if( payload.parent.scrollTop == payload.child.offsetHeight - payload.parent.offsetHeight ){
            payload.parent.scrollTop = payload.child.offsetHeight - payload.parent.offsetHeight - 1;
          };
        });
        // BLOCK REFRESH ON SCROLL
      };
    }
  }
});

export default store;
