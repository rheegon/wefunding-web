import Vue from "vue"
import config from "@/config"

const auth = {
  namespaced: true,
  state: {},
  mutations: {
    SET_AUTH( state, payload ){

      const { access_token, profile } = payload

      Object.keys( profile ).forEach( o => {
        Vue.set( state, o, profile[ o ] )
      })

      // @TODO: access token 유효성 체크

      if (access_token) {
        localStorage.setItem( config.access_token_name, access_token )
      }

    },
    UNSET_AUTH( state ){
      Object.keys( state ).forEach( o => {
        Vue.delete( state, o );
      });
      localStorage.removeItem( config.access_token_name );
    },
  },
  actions: {
    refresh({ state, rootState, commit, dispatch }){
      return new Promise( ( resolve, reject ) => {

        let accessToken = localStorage.getItem(rootState.config.access_token_name)

        if (!accessToken) {
          resolve({})
          return
        }

        Vue.axios.put( rootState.config.api2URL + "/v2/passport/auth/verify", {}, { headers: rootState.config.userHeaders }).then( res => {

          if( res.data.status == "success" ){
            commit( "SET_AUTH", res.data.data );
          }else{
            commit( "UNSET_AUTH" );
          }

          resolve( res );
        }).catch( res => {
          resolve( res );
        });
      });
    },

    redirect({ state, rootState, commit, dispatch }, payload ) {
      let { redirect } = payload

      if (rootState.auth.host_id > 0) {
        location.replace( "/host" );
      }
      else {
        if ( redirect ) {
          location.replace( redirect );
        }
        else {
          location.replace( "/" );
        }
      }

    },

    direct({ state, rootState, commit, dispatch }, payload) {
      let { token } = payload
      localStorage.setItem( config.access_token_name, token )
      location.replace( "/" )
    },

    login({ state, rootState, commit, dispatch }, payload ){
      return new Promise( ( resolve, reject ) => {
        Vue.axios.post( rootState.config.api2URL + "/v2/passport/auth/user/account", payload, { headers: rootState.config.userHeaders }).then( res => {
          if( res.data.status == "success" ){
            commit( "SET_AUTH", res.data.data );
            resolve( res );
          }else{
            reject( res );
          };
        }).catch( err => {
          reject( err );
        });
      });
    },

    logout({ state, rootState, commit, dispatch }){
      return new Promise( ( resolve, reject ) => {
        commit( "UNSET_AUTH" );
        location.replace( "/" );
        resolve();
      }).catch( err => {
        reject( err );
      });
    },

    register({ state, rootState, commit, dispatch }, payload ){
      return new Promise( ( resolve, reject ) => {
        Vue.axios.post( rootState.config.api2URL + "/v2/user/register", payload, { headers: rootState.config.userHeaders }).then( res => {
          if( res.data.status == "success" ){
            dispatch( "login", payload ).then( res => {
              resolve( res );
            }).catch( err => {
              reject( err );
            });
          }else{
            reject( res );
          };
        }).catch( err => {
          reject( err );
        });
      });
    },

    socialAccess({ state, rootState, commit, dispatch }, payload ){

      return new Promise( ( resolve, reject ) => {

      })
    },
    sendReset({ state, rootState, commit, dispatch }, payload ){
      return new Promise( ( resolve, reject ) => {
        Vue.axios.post( rootState.config.api2URL + "/v2/user/password/reset/send", { account: payload.user_email }, { headers: rootState.config.userHeaders }).then( res => {
          if( res.data.status == "success" ){
            resolve( res );
          }else{
            reject( res );
          };
        }).catch( err => {
          reject( err );
        });
      });
    },
    reset({ state, rootState, commit, dispatch }, payload ){
      return new Promise( ( resolve, reject ) => {
        Vue.axios.put( rootState.config.api2URL + "/v2/user/password/reset", payload, { headers: rootState.config.userHeaders }).then( res => {
          if( res.data.status == "success" ){
            resolve( res );
          }else{
            reject( res );
          };
        }).catch( reject );
      });
    },
    delete({ state, rootState, commit, dispatch }, payload){
      return new Promise( ( resolve, reject ) => {
        console.log( "rootState", rootState )

        Vue.axios.post( rootState.config.api2URL + "/v2/user/unregister", payload, { headers: rootState.config.userHeaders }).then( res => {
          if( res.data.status == "success" ){
            commit( "UNSET_AUTH" );
            resolve( res );
          }else{
            reject( res );
          };
        }).catch( reject );
      });
    },
    edit({ state, rootState, commit, dispatch }, payload ){
      return new Promise( ( resolve, reject ) => {
        dispatch( "formAdditionalAction", payload ).then( payloadData => {
          Vue.axios.put( rootState.config.api2URL + "/v2/passport/auth/user/profile", payloadData, { headers: rootState.config.userHeaders }).then( res => {
            if( res.data.status == "success" ){
              commit( "SET_AUTH", res.data.data );
              resolve( res );
            }else{
              reject( res );
            };
          }).catch( reject );
        });
      });
    },
    password({ state, rootState, commit, dispatch }, payload ){
      return new Promise( ( resolve, reject ) => {
        Vue.axios.put( rootState.config.api2URL + "/v2/passport/auth/user/password", payload, { headers: rootState.config.userHeaders }).then( res => {
          if( res.data.status == "success" ){
            commit( "SET_AUTH", res.data.data );
            resolve( res );
          }else{
            reject( res );
          };
        }).catch( reject );
      });
    },
    formAdditionalAction({ state }, payload ){
      return new Promise( ( resolve, reject ) => {
        if( payload.profile_photo ){
          payload.resources = {};
          payload.resources.profile_photo = JSON.parse( payload.profile_photo );
          payload.resources = JSON.stringify( payload.resources );
          Vue.delete( payload, "profile_photo" );
          resolve( payload );
        }else{
          resolve( payload );
        };
      });
    }
  }
};

export default auth;
