import Vue from "vue"

import config from "@/config"
import router from "@/router"
import store from "@/store"

import filters from "@/modules/filters.js"
/// PLUGIN
import VueMoment from "vue-moment"
import VuePortal from 'portal-vue'
import VueYoutube from 'vue-youtube'
import VueProgressiveImage from 'vue-progressive-image'
import VueSticky from "vue-sticky-directive"
import VueAxios from "vue-axios"
import axios from "axios"

const VueBrowser = {
  install (Vue, {loadComponent = true} = {}) {
    const isMobile = require('is-mobile')

    Vue.prototype.$browser = {
      isMobile: isMobile()
    }
  }
}

Vue.use( VueMoment )
Vue.use( VuePortal )
Vue.use( VueBrowser )
Vue.use( VueYoutube )
Vue.use( VueSticky )
Vue.use( VueProgressiveImage )
Vue.use( VueAxios, axios )
Vue.use( require('vue-cookies') )

//
import GlobalStyle from "@/assets/scss/site.scss"

//BUTTON ELEMENT

// FUNCTION ELEMENT

// INCLUDES


import App from "./App"

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount("#app")
