const mode = process.env.VUE_APP_MODE || process.env.NODE_ENV;

const content = require( './conf.d/config.' + mode + '.json' );

const userAccessToken = localStorage.getItem( content.access_token_name )

let userHeaders = {}

if (userAccessToken) {
  userHeaders["x-access-token"] = userAccessToken
}

const common = {
  mode: mode,
  isTouchDevice: /iPhone|iPad|iPod|Android/i.test( navigator.userAgent ),
  userHeaders: userHeaders
};

const config = {
  version: "0.0.1",
  ...content,
  ...common
};


export default config;
