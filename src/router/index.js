import Vue from "vue";
import VueRouter from "vue-router";
import VueMeta from "vue-meta";
import config from "@/config";
import store from "@/store";

Vue.use( VueRouter );

import Blank from "@/containers/Blank";
import Layout from "@/containers/Layout";

const routes = [
  { path: "/", component: Layout, children: [
    
  ]},

  { path: "*", redirect: "/" }

];

const router = new VueRouter({
  routes,
  mode: "history",
  scrollBehavior( to, from, savedPosition ){
    return savedPosition ? savedPosition : { x: 0, y: 0 };
  }
});


Vue.use(VueMeta);

export default router;
