
const path = require('path')

module.exports = {
  configureWebpack: (config) => {
    config.entry = ["./src/main.js"]
  },
  devServer: {
    disableHostCheck: true
  }
}
